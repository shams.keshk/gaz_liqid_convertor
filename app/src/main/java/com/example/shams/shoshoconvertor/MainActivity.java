package com.example.shams.shoshoconvertor;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.spinner_liqid_id)
    Spinner mSpinnerLiqidViewId;

    @BindView(R.id.et_liters_id)
    TextInputEditText mLiterEditText;

    @BindView(R.id.et_tonne_id)
    TextInputEditText mTonneEditText;

    @BindView(R.id.et_nm3_id)
    TextInputEditText mNm3EditText;

    @BindView(R.id.cbx_nm3_per_hour_id)
    CheckBox checkBoxPerHour;

    @BindView(R.id.imageView)
    ImageView imageView;

    @BindView(R.id.btn_reset_values)
    Button btnReset;

    public double mLiter ;
    public double mNm3 ;
    public double mTonne;
    private String mLiqidType;

    private TextWatcher mLiterTextWatcher;
    private TextWatcher mToneTextWatcher;
    private TextWatcher mNm3TextWatcher;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        final ScrollView scrollView = findViewById(R.id.mainLayout);

        mLiqidType = "";

        setUpLiqidTypeSpinner();

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValuesToNull();
                setViewsToNull();
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(scrollView.getWindowToken(), 0);
            }
        });

        checkBoxPerHour.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    mNm3 = mNm3 / 8 ;
                    mNm3EditText.setText(String.valueOf(mNm3));
                }else {
                    mNm3 = mNm3 * 8;
                    mNm3EditText.setText(String.valueOf(mNm3));
                }
            }
        });

     mLiterEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (mLiterTextWatcher == null) {
                    mLiterTextWatcher = new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            mTonneEditText.removeTextChangedListener(mToneTextWatcher);
                            mNm3EditText.removeTextChangedListener(mNm3TextWatcher);
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            mLiterEditText.removeTextChangedListener(mLiterTextWatcher);
                            String numberText = mLiterEditText.getText().toString();
                            if (mLiterEditText.getId() == R.id.et_liters_id) {
                                mLiterOperations(numberText);
                            } else if (mLiterEditText.getId() == R.id.et_tonne_id) {
                                mTonneOperations(numberText);
                            } else if (mLiterEditText.getId() == R.id.et_nm3_id) {
                                mNm3Operations(numberText);
                            } else {
                                return;
                            }
                            mLiterEditText.addTextChangedListener(mLiterTextWatcher);
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            mLiterEditText.setSelection(s.length());
                        }
                    };
                }
                    mLiterEditText.addTextChangedListener(mLiterTextWatcher);
                }
            }
        });


     mTonneEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                        if (mToneTextWatcher == null){
                    mToneTextWatcher = new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            mLiterEditText.removeTextChangedListener(mLiterTextWatcher);
                            mNm3EditText.removeTextChangedListener(mNm3TextWatcher);
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            mTonneEditText.removeTextChangedListener(mToneTextWatcher);
                            String numberText = mTonneEditText.getText().toString();
                            if (mTonneEditText.getId() == R.id.et_liters_id) {
                                mLiterOperations(numberText);
                            } else if (mTonneEditText.getId() == R.id.et_tonne_id) {
                                mTonneOperations(numberText);
                            } else if (mTonneEditText.getId() == R.id.et_nm3_id) {
                                mNm3Operations(numberText);
                            } else {
                                return;
                            }
                            mTonneEditText.addTextChangedListener(mToneTextWatcher);
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            mTonneEditText.setSelection(s.length());
                        }
                    };
                }
                    mTonneEditText.addTextChangedListener(mToneTextWatcher);

                }
            }
        });

        mNm3EditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    if (mNm3TextWatcher == null) {
                        mNm3TextWatcher = new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                mTonneEditText.removeTextChangedListener(mToneTextWatcher);
                                mLiterEditText.removeTextChangedListener(mLiterTextWatcher);
                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                mNm3EditText.removeTextChangedListener(mNm3TextWatcher);
                                String numberText = mNm3EditText.getText().toString();
                                if (mNm3EditText.getId() == R.id.et_liters_id) {
                                    mLiterOperations(numberText);
                                } else if (mNm3EditText.getId() == R.id.et_tonne_id) {
                                    mTonneOperations(numberText);
                                } else if (mNm3EditText.getId() == R.id.et_nm3_id) {
                                    mNm3Operations(numberText);
                                } else {
                                    return;
                                }
                                mNm3EditText.addTextChangedListener(mNm3TextWatcher);
                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                mNm3EditText.setSelection(s.length());
                            }
                        };
                    }
                    mNm3EditText.addTextChangedListener(mNm3TextWatcher);
                }
            }
        });

    }

    private double setViewZeroAndGetData(String numberText){
        if (numberText.isEmpty() || numberText.equals("")) {
                mNm3 = 0;
                mTonne = 0;
                mLiter = 0;
                return  0;

        } else {
            return Double.valueOf("0"+numberText);
        }

    }

    private void setViewsValues(TextInputEditText editText,double editValue){
        if (editValue == 0){
            editText.setText("");
        }else {
            editText.setText(String.valueOf(editValue));
        }
    }

    private void mLiterOperations(String numberText){
        mLiter = setViewZeroAndGetData(numberText);
        if (mLiqidType.equals("Oxygen")){
            caluclationOxygenAlgorithmfromLiters(mLiter);
           setViewsValues(mNm3EditText,mNm3);
            setViewsValues(mTonneEditText,mTonne);
        }else if (mLiqidType.equals("Nitrogen")){
            caluclationNitrogenAlgorithmfromLiters(mLiter);
            setViewsValues(mNm3EditText,mNm3);
            setViewsValues(mTonneEditText,mTonne);
        }else if (mLiqidType.equals("Argon")){
            caluclationArgonAlgorithmfromLiters(mLiter);
            setViewsValues(mNm3EditText,mNm3);
            setViewsValues(mTonneEditText,mTonne);
        }else {
            return;
        }
    }

    private void mTonneOperations(String numberText){
        mTonne = setViewZeroAndGetData(numberText);
        if (mLiqidType.equals("Oxygen")){
            caluclationOxygenAlgorithmfromTonne(mTonne);
            setViewsValues(mNm3EditText,mNm3);
            setViewsValues(mLiterEditText,mLiter);

        }else if (mLiqidType.equals("Nitrogen")){
            caluclationNitrogenAlgorithmfromTonne(mTonne);
            setViewsValues(mNm3EditText,mNm3);
            setViewsValues(mLiterEditText,mLiter);
        }else if (mLiqidType.equals("Argon")){
            caluclationArgonAlgorithmfromTonne(mTonne);
            setViewsValues(mNm3EditText,mNm3);
            setViewsValues(mLiterEditText,mLiter);
        }else {
            return;
        }
    }

    private void mNm3Operations(String numberText){
        mNm3 = setViewZeroAndGetData(numberText);
        if (mLiqidType.equals("Oxygen")){
            caluclationOxygenAlgorithmfromNm3(mNm3);
            setViewsValues(mTonneEditText,mTonne);
            setViewsValues(mLiterEditText,mLiter);
        }else if (mLiqidType.equals("Nitrogen")){
            caluclationNitrogenAlgorithmfromNm3(mNm3);
            setViewsValues(mTonneEditText,mTonne);
            setViewsValues(mLiterEditText,mLiter);
        }else if (mLiqidType.equals("Argon")){
            caluclationArgonAlgorithmfromNm3(mNm3);
            setViewsValues(mTonneEditText,mTonne);
            setViewsValues(mLiterEditText,mLiter);
        }else {
            return;
        }
    }

    private void setUpLiqidTypeSpinner() {
        ArrayAdapter spinnerArrayAdapter = ArrayAdapter.createFromResource(this, R.array.liqid_types,
                android.R.layout.simple_spinner_item);

        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        mSpinnerLiqidViewId.setAdapter(spinnerArrayAdapter);

        mSpinnerLiqidViewId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String selectedItem = (String) adapterView.getItemAtPosition(position);
                setViewsToNull();
                if (!TextUtils.isEmpty(selectedItem)) {
                    switch (selectedItem){
                        case "Oxygen":
                           setValuesToNull();
                            mLiqidType = "Oxygen";
                            break;
                        case "Nitrogen" :
                           setValuesToNull();
                            mLiqidType = "Nitrogen";
                            break;
                        case "Argon" :
                          setValuesToNull();
                            mLiqidType = "Argon";
                            break;
                        default:
                            mLiqidType = "Oxygen";
                            break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void setViewsToNull(){
        if (mNm3EditText != null) {
            mNm3EditText.setText("");
        }
        if (mLiterEditText != null) {
            mLiterEditText.setText("");
        }
        if (mTonneEditText != null) {
            mTonneEditText.setText("");
        }
    }

    private void setValuesToNull(){
        mNm3 = 0;
        mLiter = 0;
        mTonne = 0;
    }


    public void caluclationOxygenAlgorithmfromTonne(double tonne){
        mLiter = tonne * 876.2 ;
        mNm3 = tonne * 699.6;
    }
    public void caluclationOxygenAlgorithmfromLiters(double liter){
        mTonne = liter * 0.0011416;
        mNm3 = liter * 0.7995;
    }
    public void caluclationOxygenAlgorithmfromNm3(double nm3){

        mTonne = nm3 * 0.0014282 ;
        mLiter = nm3 * 1.2511 ;
    }

    public void caluclationNitrogenAlgorithmfromTonne(double tonne){
        mLiter = tonne * 1234.9 ;
        mNm3 = tonne * 799.6;
    }
    public void caluclationNitrogenAlgorithmfromLiters(double liter){

        mTonne = liter * 0.0008083;
        mNm3 = liter * 0.6464;
    }
    public void caluclationNitrogenAlgorithmfromNm3(double nm3){
        mTonne = nm3 * 0.0012506 ;
        mLiter = nm3 * 1.5443 ;
    }

    public void caluclationArgonAlgorithmfromTonne(double tonne){
        mLiter = tonne * 717.6 ;
        mNm3 = tonne * 560.5;
    }
    public void caluclationArgonAlgorithmfromLiters(double liter){
        mTonne = liter * 0.0013936;
        mNm3 = liter * 0.7812;
    }
    public void caluclationArgonAlgorithmfromNm3(double nm3){
        mTonne = nm3 * 0.001784 ;
        mLiter = nm3 * 1.2802 ;
    }


}
